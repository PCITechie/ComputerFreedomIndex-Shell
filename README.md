# ComputerFreedomIndex-Shell


## About
Shell script for evaluating how free (libre) your system is. Takes hardware, operating system, and installed applications into account. Licensed under the GPLv3 or, optionally, any later version, see the COPYING file or https://www.gnu.org/licenses/gpl-3.0.txt. Copyright (c) 2021 PCITechie.

## Warranty Disclaimer
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
